<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Users CRUD App made in CakePHP 3</title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css(['base.css', 'cake.css', 'custom.css']) ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                <?= $this->Html->link('Users/Roles CRUD App', '/users') ?>
            </div>

            <div class="links">
                <?= $this->Html->link('Cinkciarz', 'https://cinkciarz.pl', ['target' => '_blank']); ?>
                <?= $this->Html->link('CakePHP Framework', 'https://cakephp.org', ['target' => '_blank']); ?>
                <?= $this->Html->link('My LinkedIn', 'https://www.linkedin.com/in/spayder', ['target' => '_blank']); ?>
                <?= $this->Html->link('My Gitlab', 'https://gitlab.com/users/spayder', ['target' => '_blank']); ?>
            </div>
        </div>
    </div>
</body>
</html>

# CinkUsers - simple CRUD app

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

A simple CRUD applications made with [CakePHP](http://cakephp.org) 3.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Clone project with Git or download it and unzip
```bash
git clone git@gitlab.com:spayder/cinkusers.git [app_name]
```

2. Visit the path to where the project is installed and run `php composer.phar install`.

If Composer is installed globally, run:
```bash
composer install
```

3. In your database create a new empty schema i.e. 
```
create schema cinkusers;
```

## Configuration

1. Read and edit `config/app.php` and setup the `'Datasources'` and any other
configuration relevant for your application.

2. Migrate database. From the path where the project is installed, run:
```bash
bin/cake migrations migrate
```

3. Seed database. Run:
```bash
bin/cake migrations seed
```